{

	description = "MMenu... delicious";

	outputs = { self, nixpkgs }: {

		defaultPackage.x86_64-linux =
			with import nixpkgs { system = "x86_64-linux"; };
			haskellPackages.mkDerivation {
				pname = "mmenu";
				version = "v0.1.1.1";
				src = self;
				isExecutable = true;
				executableHaskellDepends = with haskellPackages; [ base containers directory filepath split xdg-desktop-entry ];
				license = lib.licenses.gpl3;
			};

		overlay = final: prev: {
			mmenu = self.defaultPackage.x86_64-linux;
		};

	};

}
