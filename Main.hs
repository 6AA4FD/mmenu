-- scan for .desktop entries and print a line-separated list of commands
-- filter with fzf
import           System.Environment.XDG.DesktopEntry
                                               as X
import           System.IO
import           System.Directory
import           System.FilePath.Posix
import           Data.Maybe
import           Control.Monad
--import           Control.Conditional
--import           Database.Redis                as R

preferredLanguages :: [String]
preferredLanguages = ["en"]

formatEntry :: DesktopEntry -> Maybe String
formatEntry a =
  let b = X.deCommand a
  in  if isJust b
        then Just $ X.deName preferredLanguages a ++ ": " ++ fromJust b
        else Nothing

listCommands = catMaybes <$> (map formatEntry) <$> X.getDirectoryEntriesDefault

item :: IO String
item = (concatMap (\x -> x ++ "\n")) <$> liftM (\x -> dedup x []) listCommands

dedup (x : xs) y | elem x y  = dedup xs y
                 | otherwise = dedup xs $ y ++ [x]
dedup [] y = y

main = putStr =<< item

fileList =
  flatten
    <$> (   fmap (</> "applications")
        <$> getXDGDataDirs
        >>= filterM (doesDirectoryExist)
        >>= mapM getAbsContents
        )

getAbsContents d = map (d </>) <$> listDirectory d

flatten xs = (\z n -> foldr (\x y -> foldr z y x) n xs) (:) [] -- taken from SE lol

foo = fileList >>= mapM readDesktopEntry

--readEntry x = formatEntry <$> formatEntry x
